from __future__ import print_function

import sys
import os
import numpy as np
import random
import math
import glob


import torch
import torchvision.transforms as transforms
from torch.autograd import Variable
from torch.nn.parallel import DataParallel
import cPickle as pickle
import time
import argparse
from PIL import Image, ImageFont, ImageDraw

from baseline.model.DeepMAR import DeepMAR_ResNet50
from baseline.utils.utils import str2bool
from baseline.utils.utils import save_ckpt, load_ckpt
from baseline.utils.utils import load_state_dict 
from baseline.utils.utils import set_devices
from baseline.utils.utils import set_seed


dataset = pickle.load(open('./dataset/peta/peta_dataset.pkl'))

class Config(object):
    def __init__(self):
        
        # gpu ids
        self.sys_device_ids = (0, )

        # random
        self.set_seed = None
        
        self.resize = (224, 224)
        self.mean = [0.485, 0.456, 0.406]
        self.std = [0.229, 0.224, 0.225]
    
        # utils
        self.load_model_weight = True
        self.model_weight_file = './exp/deepmar_resnet50/peta/partition0/run1/model/ckpt_epoch50.pth'
        if self.load_model_weight:
            if self.model_weight_file == '':
                print('Please input the model_weight_file if you want to load model weight')
                raise ValueError
        
        # dataset 
        dataset_name = './dataset/peta/peta_dataset.pkl'
        dataset = pickle.load(open(dataset_name))
        self.att_list = [dataset['att_name'][i] for i in dataset['selected_attribute']]
        
        # model
        model_kwargs = dict()
        model_kwargs['num_att'] = len(self.att_list)
        model_kwargs['last_conv_stride'] = [1,2]
        self.model_kwargs = model_kwargs
        
        
### main function ###
cfg = Config()


# set the random seed
if cfg.set_seed:
    set_seed( cfg.rand_seed )
# init the gpu ids
set_devices(cfg.sys_device_ids)

# dataset 
normalize = transforms.Normalize(mean=cfg.mean, std=cfg.std)
test_transform = transforms.Compose([
        transforms.Resize(cfg.resize),
        transforms.ToTensor(),
        normalize,])

### Att model ###
model = DeepMAR_ResNet50(**cfg.model_kwargs)

# load model weight if necessary
if cfg.load_model_weight:
    map_location = (lambda storage, loc:storage)
    ckpt = torch.load(cfg.model_weight_file, map_location=map_location)
    model.load_state_dict(ckpt['state_dicts'][0])

model.cuda()
model.eval()

files = glob.glob('./dataset/cut/*.png')
# load one image 
for i, img_link in tqdm_notebook(enumerate(files), total=(len(files))):
    t1 = time.time()
    
    img = Image.open(img_link)
    width, height = img.size
    img_trans = test_transform( img ) 
    img_trans = torch.unsqueeze(img_trans, dim=0)
    img_var = Variable(img_trans).cuda()
    score = model(img_var).data.cpu().numpy()
    
    t2 = time.time()
    # show the score in command line
#     for idx in range(len(cfg.att_list)):
#         if score[0, idx] >= 0:
#             print '%s: %.2f'%(cfg.att_list[idx], score[0, idx])

    # show the score in the image
    img = img.resize(size=(width, height), resample=Image.BILINEAR)
    draw = ImageDraw.Draw(img)
    draw_text = 0
    for idx in range(len(cfg.att_list)) :
        if score[0, idx] >= 3.0 and cfg.att_list[idx] == u'personalMale': 
            txt = u"Male"
            draw.text((10, 10 ), txt, (255, 0, 0))
            draw_text +=1
            
        elif score[0, idx] <= 3.0 and cfg.att_list[idx] == u'personalMale':
            txt = u'Female'
            draw.text((10, 10), txt, (255, 0, 0))
            draw_text +=1
    
    if draw_text == 0:
        txt = u'Female'
        draw.text((20, 20), txt, (255, 0, 0))

    img.save('./dataset/cut/results_by_peta_weights/image_result_%i.png'%(i))
    
    print('Inference time: {:.3f}s'.format(t2 - t1))
    print('Total time: {:.3f}s'.format(time.time() - t1))
        